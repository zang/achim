# achim

A minimalist beamer theme borrowing liberally from
https://github.com/FuzzyWuzzie/Beamer-Theme-Execushares.

The theme provides the =\Citetext= macro for inserting citations at
the bottom of a slide.

