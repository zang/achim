DESTDIR     ?= $(shell kpsewhich -var-value=TEXMFHOME)
INSTALL_DIR = $(DESTDIR)/tex/latex/achim
PACKAGE_STY = beamerthemeachim.sty

demo:
	xelatex --shell-escape demo.tex

clean:
	rm *.{log,aux,out,nav,vrb,snm,toc,log}
install:
	@mkdir -p $(INSTALL_DIR)
	@cp $(PACKAGE_STY) $(INSTALL_DIR)
